package com.cloud.gestion.metier;

import java.util.List;

import com.cloud.gestion.entities.*;

public interface IMetier {
	public Section addSection(Section c);
	public List<Section> listSections();
	public Filiere addFiliere(Filiere f);
	public List<Filiere> listFilieres();
	public Matiere addMatiere(Matiere m);
	public List<Matiere> listMatieres();
	public Professeur addProf(Professeur p);
	public List<Professeur> listProfesseurs();
	public Semestre addSemestre(Semestre s);
	public List<Semestre> listSemestres();
	public Creneau addCreneau(Creneau cre);
	public List<Creneau> listCreneaux();
	

}
