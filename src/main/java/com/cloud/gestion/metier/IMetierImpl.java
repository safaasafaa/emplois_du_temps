package com.cloud.gestion.metier;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cloud.gestion.dao.ICreneauRepository;
import com.cloud.gestion.dao.IFiliereRepository;
import com.cloud.gestion.dao.IMatiereRepository;
import com.cloud.gestion.dao.IProfesseurRepository;
import com.cloud.gestion.dao.ISectionRepository;
import com.cloud.gestion.dao.ISemestreRepository;
import com.cloud.gestion.entities.Creneau;
import com.cloud.gestion.entities.Filiere;
import com.cloud.gestion.entities.Matiere;
import com.cloud.gestion.entities.Professeur;
import com.cloud.gestion.entities.Section;
import com.cloud.gestion.entities.Semestre;

@Service
public class IMetierImpl implements IMetier {
	@Autowired
	private ISectionRepository sectionrep;
	@Autowired
	private ICreneauRepository creneaurep;
	@Autowired
	private IMatiereRepository matiererep;
	@Autowired
	private IFiliereRepository filiererep;
	@Autowired
	private ISemestreRepository semestrerep;
	@Autowired
	private IProfesseurRepository profrep;
	
	

	@Override
	public Section addSection(Section c) {
		// TODO Auto-generated method stub
		
		return sectionrep.save(c);
	}

	@Override
	public List<Section> listSections() {
		// TODO Auto-generated method stub
		return sectionrep.findAll();
	}

	@Override
	public Filiere addFiliere(Filiere f) {
		// TODO Auto-generated method stub
		return filiererep.save(f);
	}

	@Override
	public List<Filiere> listFilieres() {
		// TODO Auto-generated method stub
		return filiererep.findAll();
	}

	@Override
	public Matiere addMatiere(Matiere m) {
		// TODO Auto-generated method stub
		return matiererep.save(m);
	}

	@Override
	public List<Matiere> listMatieres() {
		// TODO Auto-generated method stub
		return matiererep.findAll();
	}

	@Override
	public Professeur addProf(Professeur p) {
		// TODO Auto-generated method stub
		return profrep.save(p);
	}

	@Override
	public List<Professeur> listProfesseurs() {
		// TODO Auto-generated method stub
		return profrep.findAll();
	}

	@Override
	public Semestre addSemestre(Semestre s) {
		// TODO Auto-generated method stub
		return semestrerep.save(s);
	}

	@Override
	public List<Semestre> listSemestres() {
		// TODO Auto-generated method stub
		return semestrerep.findAll();
	}

	@Override
	public Creneau addCreneau(Creneau cre) {
		// TODO Auto-generated method stub
		return creneaurep.save(cre);
	}

	@Override
	public List<Creneau> listCreneaux() {
		// TODO Auto-generated method stub
		return creneaurep.findAll();
	}

}
