package com.cloud.gestion.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.cloud.gestion.entities.Section;


public interface ISectionRepository extends JpaRepository<Section,Long>{
	@Query("select s from Section s where s.NumeroSection = :x ")
	  Section findByNum(@Param("x") Long num);

}
