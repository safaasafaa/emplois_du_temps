package com.cloud.gestion.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.cloud.gestion.entities.Creneau;
import com.cloud.gestion.entities.Filiere;

public interface ICreneauRepository extends JpaRepository<Creneau,Long>{
	
	@Query("select s from Creneau s where s.jour = :x ")
	  Creneau findByJour(@Param("x") String desc);

	@Query("select s from Creneau s where s.idCre = :x ")
	  Creneau findByIdCre(@Param("x") Long id);
}
