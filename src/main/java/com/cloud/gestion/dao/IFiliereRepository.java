package com.cloud.gestion.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.cloud.gestion.entities.Filiere;


public interface IFiliereRepository extends JpaRepository<Filiere,Long>{

	@Query("select s from Filiere s where s.description = :x ")
	  Filiere findByDesc(@Param("x") String desc);
}
