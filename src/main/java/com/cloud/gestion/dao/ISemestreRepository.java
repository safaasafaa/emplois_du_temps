package com.cloud.gestion.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.cloud.gestion.entities.Semestre;

public interface ISemestreRepository extends JpaRepository<Semestre,Long>{
	
	@Query("select s from Semestre s where s.NumeroSemestre = :x ")
	  Semestre findByNum(@Param("x") Long num);

}
