package com.cloud.gestion.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.cloud.gestion.entities.Creneau;
import com.cloud.gestion.entities.Matiere;

public interface IMatiereRepository extends JpaRepository<Matiere,Long>{

	@Query("select s from Matiere s where s.description = :x ")
	  Matiere findByDesc(@Param("x") String desc);
}
