package com.cloud.gestion.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.cloud.gestion.entities.Creneau;
import com.cloud.gestion.entities.Professeur;

public interface IProfesseurRepository extends JpaRepository<Professeur,Long>{

	@Query("select s from Professeur s where s.nom = :x ")
	  Professeur findByNom(@Param("x") String desc);
}
