package com.cloud.gestion.model;

public class ModelSection {
	
	private Long NumeroSection;
	private Long NumeroSemestre;
	private String descFil;
	private Long idCre;
	public ModelSection() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Long getNumeroSection() {
		return NumeroSection;
	}
	public void setNumeroSection(Long numeroSection) {
		NumeroSection = numeroSection;
	}
	public Long getNumeroSemestre() {
		return NumeroSemestre;
	}
	public void setNumeroSemestre(Long numeroSemestre) {
		NumeroSemestre = numeroSemestre;
	}
	public String getDescFil() {
		return descFil;
	}
	public void setDescFil(String descFil) {
		this.descFil = descFil;
	}
	public Long getIdCre() {
		return idCre;
	}
	public void setIdCre(Long idCre) {
		this.idCre = idCre;
	}
	public ModelSection(Long numeroSection, Long numeroSemestre, String descFil, Long idCre) {
		super();
		NumeroSection = numeroSection;
		NumeroSemestre = numeroSemestre;
		this.descFil = descFil;
		this.idCre = idCre;
	}
	
	
	

}
