package com.cloud.gestion.model;

public class ModelCreneau {
	
	private String jour;
	private String heureDebut;
	private String heureFin;
	private String descMat;
	private String Nom;
	private String desFil;
	public ModelCreneau(String jour, String heureDebut, String heureFin, String descMat, String nom, String desFil,
			Long numeroSection) {
		super();
		this.jour = jour;
		this.heureDebut = heureDebut;
		this.heureFin = heureFin;
		this.descMat = descMat;
		Nom = nom;
		this.desFil = desFil;
		NumeroSection = numeroSection;
	}
	public String getDesFil() {
		return desFil;
	}
	public void setDesFil(String desFil) {
		this.desFil = desFil;
	}
	private Long NumeroSection;
	public String getJour() {
		return jour;
	}
	public void setJour(String jour) {
		this.jour = jour;
	}
	public String getHeureDebut() {
		return heureDebut;
	}
	public void setHeureDebut(String heureDebut) {
		this.heureDebut = heureDebut;
	}
	public String getHeureFin() {
		return heureFin;
	}
	public void setHeureFin(String heureFin) {
		this.heureFin = heureFin;
	}
	public String getDescMat() {
		return descMat;
	}
	public void setDescMat(String descMat) {
		this.descMat = descMat;
	}
	public String getNom() {
		return Nom;
	}
	public void setNom(String nom) {
		Nom = nom;
	}
	public Long getNumeroSection() {
		return NumeroSection;
	}
	public void setNumeroSection(Long numeroSection) {
		NumeroSection = numeroSection;
	}
	public ModelCreneau(String jour, String heureDebut, String heureFin, String descMat, String nom,
			Long numeroSection) {
		super();
		this.jour = jour;
		this.heureDebut = heureDebut;
		this.heureFin = heureFin;
		this.descMat = descMat;
		Nom = nom;
		NumeroSection = numeroSection;
	}
	public ModelCreneau() {
		super();
	}
	
	

}
