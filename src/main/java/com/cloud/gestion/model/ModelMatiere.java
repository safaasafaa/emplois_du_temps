package com.cloud.gestion.model;

import java.io.Serializable;

public class ModelMatiere {
	
	//private IFiliereRepository filRep;
	

	private String descMat;
	private Long NumeroSemestre;
	private String descFil;
	public ModelMatiere() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public String getDescMat() {
		return descMat;
	}
	public void setDescMat(String descMat) {
		this.descMat = descMat;
	}
	public Long getNumeroSemestre() {
		return NumeroSemestre;
	}
	public void setNumeroSemestre(Long numeroSemestre) {
		NumeroSemestre = numeroSemestre;
	}
//	public String getDescSem() {
//		return descSem;
//	}
//	public void setDescSem(String descSem) {
//		this.descSem = descSem;
//	}
	public String getDescFil() {
		return descFil;
	}
	public void setDescFil(String descFil) {
		this.descFil = descFil;
	}
	public ModelMatiere(String descMat, Long numeroSemestre, String descSem, String descFil) {
		super();
	
		this.descMat = descMat;
		NumeroSemestre = numeroSemestre;
		//this.descSem = descSem;
		this.descFil = descFil;
	}
	
	

}
