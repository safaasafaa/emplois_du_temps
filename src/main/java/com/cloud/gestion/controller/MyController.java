package com.cloud.gestion.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.cloud.gestion.dao.ICreneauRepository;
import com.cloud.gestion.dao.IFiliereRepository;
import com.cloud.gestion.dao.IMatiereRepository;
import com.cloud.gestion.dao.IProfesseurRepository;
import com.cloud.gestion.dao.ISectionRepository;
import com.cloud.gestion.dao.ISemestreRepository;
import com.cloud.gestion.entities.Creneau;
import com.cloud.gestion.entities.Filiere;
import com.cloud.gestion.entities.Matiere;
import com.cloud.gestion.entities.Professeur;
import com.cloud.gestion.entities.Section;
import com.cloud.gestion.entities.Semestre;
import com.cloud.gestion.model.ModelCreneau;
import com.cloud.gestion.model.ModelMatiere;
import com.cloud.gestion.model.ModelSection;


@Controller

public class MyController {
	@Autowired
	private IFiliereRepository filRepo;
	
	@Autowired
	private ISemestreRepository semRepo;
	
	@Autowired
	private IProfesseurRepository profRepo;
	
	@Autowired
	private IMatiereRepository matRepo;
	
	@Autowired
	private ISectionRepository secRepo;
	
	@Autowired
	private ICreneauRepository creRepo;
	
	@RequestMapping(value="/index")
	public String Index() {
		return "index";
	}
	
	
	@RequestMapping(value = "/ajoutFiliere")
	public String afficherFiliere(Model model) {
		Filiere f=new Filiere();
	
		model.addAttribute("FiliereForm",f);
		return "addFiliere";
	}
	@RequestMapping(value = "/addFiliere", method = RequestMethod.POST)
    public String saveFiliere(Filiere f){
        filRepo.save(f);
        return "redirect:/index";
  }
	
	@RequestMapping(value = "/ajoutSection")
	public String afficherSection(Model model) {
		ModelSection s=new ModelSection();
		model.addAttribute("listeSemestres",semRepo.findAll());
		model.addAttribute("listeFilieres",filRepo.findAll());
		model.addAttribute("listeCreneaux",creRepo.findAll());
		model.addAttribute("SectionForm",s);
		return "addSection";
	}
	@RequestMapping(value = "/ajoutMatiere")
	public String afficherMatiere(Model model) {
		ModelMatiere m=new ModelMatiere();
		model.addAttribute("listeSemestres",semRepo.findAll());
		model.addAttribute("listeFilieres",filRepo.findAll());
		
		model.addAttribute("MatiereForm",m);
		return "addMatiere";
	}
	@RequestMapping(value = "/addSection", method = RequestMethod.POST)
    public String saveSection(ModelSection m){
		//Long id = new Long(m.);
		Semestre s=semRepo.findByNum(m.getNumeroSemestre());
		//semRepo.save(s);
		
		Filiere f=filRepo.findByDesc(m.getDescFil());
		//filRepo.save(f);
		Creneau c=creRepo.findByIdCre(m.getIdCre());
	
		Section sec=new Section(m.getNumeroSection(),s,f,c);
		secRepo.save(sec);
		
        return "redirect:/index";
  }
	
	@RequestMapping(value = "/addMatiere", method = RequestMethod.POST)
    public String saveMatiere(ModelMatiere m){
		//Long id = new Long(m.);
		Semestre s=semRepo.findByNum(m.getNumeroSemestre());
		semRepo.save(s);
		
		Filiere f=filRepo.findByDesc(m.getDescFil());
		filRepo.save(f);
		Matiere mat=new Matiere(m.getDescMat(),s,f);
		matRepo.save(mat);
        return "redirect:/index";
  }
	
	@RequestMapping(value = "/ajoutProfesseur")
	public String afficherProfesseur(Model model) {
		Professeur p=new Professeur();
		model.addAttribute("ProfesseurForm",p);
		return "addProfesseur";
	}
	@RequestMapping(value = "/addProfesseur", method = RequestMethod.POST)
    public String saveProfesseur(Professeur p){
		profRepo.save(p);
        return "redirect:/index";
  }
	
	@RequestMapping(value = "/ajoutCreneau")
	public String afficherCreneau(Model model) {
		ModelCreneau c=new ModelCreneau();
		List<String> jours=new ArrayList<String>();
			jours.add("Lundi");
			jours.add("Mardi");
			jours.add("Mercredi");
			jours.add("Jeudi");
			jours.add("Vendredi");
			model.addAttribute("listeJours",jours);
			List<String> heures=new ArrayList<String>();
			heures.add("8:30");
			heures.add("9:00");
			heures.add("9:30");
			heures.add("10:00");
			heures.add("10:30");
			heures.add("11:00");
			model.addAttribute("listeHeures",heures);
		model.addAttribute("listeMatieres",matRepo.findAll());
		model.addAttribute("listeProfesseurs",profRepo.findAll());
		model.addAttribute("listeSections",secRepo.findAll());
		model.addAttribute("listeFilieres",filRepo.findAll());
		model.addAttribute("CreneauForm",c);
		return "addCreneau";
	}
	
	@RequestMapping(value = "/addCreneau", method = RequestMethod.POST)
    public String saveCreneau(ModelCreneau m){
		
		
		Filiere f=filRepo.findByDesc(m.getDesFil());
		Matiere mat=matRepo.findByDesc(m.getDescMat());
		Section sec=secRepo.findByNum(m.getNumeroSection());
		Professeur prof=profRepo.findByNom(m.getNom());
		
		Creneau c=new Creneau(m.getJour(),m.getHeureDebut(),m.getHeureFin(),prof,mat);
		creRepo.save(c);
	
        return "redirect:/index";
  }
	
	@RequestMapping(value = "/ajoutSemestre")
	public String afficherSemestre(Model model) {
		Semestre s=new Semestre();
		model.addAttribute("SemestreForm",s);
		return "addSemestre";
	}
	@RequestMapping(value = "/addSemestre", method = RequestMethod.POST)
    public String saveSemestre(Semestre s){
        semRepo.save(s);
        return "redirect:/index";
  }
	
	@RequestMapping(value = "/afficher", method = RequestMethod.GET)
    public String afficher(Model model){
		List<String> heures=new ArrayList<String>();
		heures.add("8:30");
		heures.add("9:00");
		heures.add("9:30");
		heures.add("10:00");
		heures.add("10:30");
		heures.add("11:00");
		heures.add("11:30");
		heures.add("12:00");
		heures.add("12:30");
		heures.add("13:00");
		heures.add("13:30");
		heures.add("14:00");
		heures.add("14:30");
		heures.add("15:00");
		heures.add("15:30");
		heures.add("16:00");
		heures.add("17:30");
		heures.add("18:00");
		heures.add("18:30");
		model.addAttribute("listeHeures",heures);
       
        return "emploi";
  }
	
	
}
