package com.cloud.gestion.entities;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;


@Entity
public class Section implements Serializable{

	@Id
	private Long NumeroSection;
	
	@ManyToOne
	@JoinColumn(name="idCre")
	private Creneau creneau;
	
	@ManyToOne
	@JoinColumn(name="numSemestre")
	private Semestre semestre;
	
	@ManyToOne
	@JoinColumn(name="idFiliere")
	private Filiere filiere;

	public Section() {
		super();
		// TODO Auto-generated constructor stub
	}

	
	
	public Section(Long numeroSection, Semestre semestre, Filiere filiere,Creneau creneau) {
		super();
		NumeroSection = numeroSection;
		this.semestre = semestre;
		this.filiere = filiere;
		this.creneau=creneau;
	}



	public Long getNumeroSection() {
		return NumeroSection;
	}

	public void setNumeroSection(Long numeroSection) {
		NumeroSection = numeroSection;
	}

	public Creneau getCreneau() {
		return creneau;
	}

	public void setCreneau(Creneau creneau) {
		this.creneau = creneau;
	}

	public Semestre getSemestre() {
		return semestre;
	}

	public void setSemestre(Semestre semestre) {
		this.semestre = semestre;
	}

	public Filiere getFiliere() {
		return filiere;
	}

	public void setFiliere(Filiere filiere) {
		this.filiere = filiere;
	}
	
	
}
