package com.cloud.gestion.entities;


import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
@Entity
public class Creneau implements Serializable{
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long idCre;
	private String jour;
	private String heureDebut;
	private String heureFin;
	
	@OneToMany(mappedBy="creneau",fetch=FetchType.LAZY)
	private List<Section> sections;
	
	@ManyToOne
	@JoinColumn(name="idProf")
	private Professeur prof;
	
	@ManyToOne
	@JoinColumn(name="idMatiere")
	private Matiere mat;

	public Creneau() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Long getIdCre() {
		return idCre;
	}

	public void setIdCre(Long idCre) {
		this.idCre = idCre;
	}

	public String getJour() {
		return jour;
	}

	public void setJour(String jour) {
		this.jour = jour;
	}

	public String getHeureDebut() {
		return heureDebut;
	}

	public void setHeureDebut(String heureDebut) {
		this.heureDebut = heureDebut;
	}

	public String getHeureFin() {
		return heureFin;
	}

	public void setHeureFin(String heureFin) {
		this.heureFin = heureFin;
	}

	public List<Section> getSections() {
		return sections;
	}

	public void setSections(List<Section> sections) {
		this.sections = sections;
	}

	public Professeur getProf() {
		return prof;
	}

	public void setProf(Professeur prof) {
		this.prof = prof;
	}

	public Matiere getMat() {
		return mat;
	}

	public void setMat(Matiere mat) {
		this.mat = mat;
	}

	public Creneau(String jour, String heureDebut, String heureFin, Professeur prof,
			Matiere mat) {
		super();
		this.jour = jour;
		this.heureDebut = heureDebut;
		this.heureFin = heureFin;
	
		this.prof = prof;
		this.mat = mat;
	}
	
	

}
