package com.cloud.gestion.entities;


import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
@Entity
public class Semestre implements Serializable{
	@Id
	private Long NumeroSemestre;
	private String description;

	public Semestre() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Long getNumeroSemestre() {
		return NumeroSemestre;
	}

	public void setNumeroSemestre(Long numeroSemestre) {
		NumeroSemestre = numeroSemestre;
	}
	
	

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Semestre(Long numeroSemestre,String description) {
		super();
		NumeroSemestre = numeroSemestre;
		this.description=description;
	}
	
	
	
//	@OneToMany
//	private String description;
//	
//	@OneToMany
//	private List<Section> sections;
	
	

}
