package com.cloud.gestion.entities;


import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
@Entity
public class Matiere implements Serializable{
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long idMat;
	
	private String description;
	
	@ManyToOne
	@JoinColumn(name="idSemestre")
	private Semestre semestre;
	
	@ManyToOne
	@JoinColumn(name="idFiliere")
	private Filiere filiere;

	public Matiere() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Long getIdMat() {
		return idMat;
	}

	public void setIdMat(Long idMat) {
		this.idMat = idMat;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Semestre getSemestre() {
		return semestre;
	}

	public void setSemestre(Semestre semestre) {
		this.semestre = semestre;
	}

	public Filiere getFiliere() {
		return filiere;
	}

	public void setFiliere(Filiere filiere) {
		this.filiere = filiere;
	}

	public Matiere(String description) {
		super();
		this.description = description;
	}

	public Matiere(String description, Semestre semestre, Filiere filiere) {
		super();
		this.description = description;
		this.semestre = semestre;
		this.filiere = filiere;
	}
	
	
	

}
