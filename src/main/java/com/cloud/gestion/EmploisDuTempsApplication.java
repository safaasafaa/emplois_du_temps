package com.cloud.gestion;

import java.util.List;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

import com.cloud.gestion.dao.ICreneauRepository;
import com.cloud.gestion.dao.IFiliereRepository;
import com.cloud.gestion.dao.IMatiereRepository;
import com.cloud.gestion.dao.IProfesseurRepository;
import com.cloud.gestion.dao.ISectionRepository;
import com.cloud.gestion.dao.ISemestreRepository;
import com.cloud.gestion.entities.Creneau;
import com.cloud.gestion.entities.Filiere;
import com.cloud.gestion.entities.Matiere;
import com.cloud.gestion.entities.Professeur;
import com.cloud.gestion.entities.Section;
import com.cloud.gestion.entities.Semestre;


@SpringBootApplication
public class EmploisDuTempsApplication {

	public static void main(String[] args) {
		ApplicationContext ctx=SpringApplication.run(EmploisDuTempsApplication.class, args);
		
		IFiliereRepository filiereRepository=ctx.getBean(IFiliereRepository.class);
		Filiere f= filiereRepository.save(new Filiere("SMI"));
		
		ISemestreRepository semestreRepository=ctx.getBean(ISemestreRepository.class);
		Semestre sem=semestreRepository.save(new Semestre(new Long(4),"semestre"));
		
		IMatiereRepository matiereRepository=ctx.getBean(IMatiereRepository.class);
		Matiere mat=matiereRepository.save(new Matiere("xml", sem, f));
		
		IProfesseurRepository professeurRepository=ctx.getBean(IProfesseurRepository.class);
		Professeur prof1=professeurRepository.save(new Professeur("XXX", "YYY", "Informatique", "ZZZ"));
		
		
		ICreneauRepository creneauRepository=ctx.getBean(ICreneauRepository.class);
		Creneau creneau1=creneauRepository.save(new Creneau("lundi", "8h30", "9h00",prof1,mat));
		Creneau creneau2=creneauRepository.save(new Creneau("lundi", "9h00", "9h30",prof1,mat));
		
		ISectionRepository sectionRepository=ctx.getBean(ISectionRepository.class);
		Section sec1=sectionRepository.save(new Section(new Long(1), sem, f,creneau1));
	
		Section sec2=sectionRepository.save(new Section(new Long(2), sem, f,creneau2));
		
		
		
		System.out.println(semestreRepository.findByNum(new Long(4)));
		System.out.println(filiereRepository.findByDesc("SMI"));
	}
}
